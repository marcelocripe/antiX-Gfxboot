#!/bin/bash

# usage
[ x${1} != x${1#-} ] && cat<<'USAGE' && exit

tx-push-extra.sh - push 'xtra' po-files to transifex 
             for antix-development.live-grubpot

Usage: tx-push-extra.sh 

   to push xtra/*.po files.

Note: If TX_TOKEN is unset, it will ask to enter (will not be saved).
      Leave empty (press enter) to get transifex authentication prompt.

USAGE

# chage to ./xtra diretory 

cd xtra  || exit 

# create transifex .tx/config
[ -d .tx ] || mkdir .tx

cat <<EOF > .tx/config
[main]
host = https://www.transifex.com

[antix-development.live-grubpot]
file_filter = ./<lang>.po
minimum_perc = 0
source_file = ../en.pot
source_lang = en
type = PO
EOF

# transifex authentication check

if ! grep -sq password  ~/.transifexrc; then
    if [[ -z ${TX_TOKEN}  ]]; then
        echo "Please enter api token here, will not be saved to ~/.transifexrc."
        echo "Leave empty [press enter] to get it saved when asked again"
        read -r -s -p "Enter your api token: " TX_TOKEN
        export TX_TOKEN
        [[ -z ${TX_TOKEN}  ]] && unset TX_TOKEN
        echo
    fi
fi

#echo tx push --translations --language $(echo *.po|sed  's/[.]po//g; s/ /,/g')
#tx push --translations --language $(echo *.po|sed  's/[.]po//g; s/ /,/g')

echo tx push --force --translations --language $(echo *.po|sed  's/[.]po//g; s/ /,/g')
tx push --force --translations --language $(echo *.po|sed  's/[.]po//g; s/ /,/g')

