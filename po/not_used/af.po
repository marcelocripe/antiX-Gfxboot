# LANGUAGE translations for boot loader
# Copyright (C) 2005 SUSE Linux GmbH
#
msgid ""
msgstr ""
"Project-Id-Version: bootloader\n"
"POT-Creation-Date: 2006-10-26 15:50+0200\n"
"PO-Revision-Date: 2006-11-03 14:26\n"
"Last-Translator: Novell Language <language@novell.com>\n"
"Language-Team: Novell Language <language@novell.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. ok button label
#. txt_ok
msgid "OK"
msgstr "Goed"

#. cancel button label
#. txt_cancel
msgid "Cancel"
msgstr "Kanselleer"

#. txt_boot_harddisk
msgid "Boot from Hard Disk"
msgstr "Selflaai vanaf harde skyf"

#. txt_memtest
msgid "Memory Test"
msgstr "Geheuetoets"

#. txt_bootoptions
msgid "Boot Options"
msgstr "Selflaai-opsies"

#. window title for exit dialog
#. txt_exit_title (see txt_exit_dialog)
msgid "Exiting..."
msgstr "Hersien van ..."

#. txt_help
msgid "Help"
msgstr "Help"

#. power off dialog title
#. txt_power_off_title
msgid "Power Off"
msgstr "Krag af"

#. txt_power_off
msgid "Halt the system now?"
msgstr "Halt die stelsel nou?"

#. label for language selection
#. txt_language
msgid "Language"
msgstr "Taal"

#. video mode/display size menu title
#. ** please keep it really short (comparable to the english text) **
#. txt_video_mode
msgid "Video Mode"
msgstr ""

#. Like automatic, not automobile.
#. txt_auto
msgid "auto"
msgstr ""

#. Perform md5 check. "md5" should not be translated?
#. txt_check_md5
msgid "check md5"
msgstr ""

#. Short for "command line".  Please keep it short.
#. txt_cmd_line
msgid "cmd. line"
msgstr ""

#. Boot directly into command line (for low memory systems).
#. txt_command_line_install
msgid "Command Line Install"
msgstr ""

#. Title for console resolution menu.  Please keep it short.
#. txt_console
msgid "Console"
msgstr ""

#. As in "the default option".  Please keep it short.
#. txt_default
msgid "default"
msgstr ""

#. Title of menu to select desktop program.
#. txt_desktop
msgid "Desktop"
msgstr ""

#. Boot with home persistence enabled.
#. txt_home_persistence
msgid "Home Persistence"
msgstr ""

#. Default for Options menu.  Please keep it short.
#. txt_none
msgid "none"
msgstr ""

#. Enable automatic mounting.
#. txt_automount
msgid "auto mount"
msgstr ""

#. Copy everything to RAM for faster running.
#. txt_to_ram
msgid "to ram"
msgstr ""

#. Title of Options menu.  Please keep it short.
#. txt_options
msgid "Options"
msgstr ""

#. Boot with root persistence enabled.
#. txt_root_persistence
msgid "Root Persistence"
msgstr ""

#. Title of Time Zone menu.
#. txt_timezone
msgid "Timezone"
msgstr ""

